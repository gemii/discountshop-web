/**
 * Created by mavis on 2017/5/26.
 */
var openid;
var type = getQueryString("info").split('-')[3];//8
var code =getQueryString("code");
var controller={
    // 页面初始化，使最外层的div的高度=window的高度
    init:function () {
        document.getElementById("cardView").style.height=document.documentElement.clientHeight+"px";
        this.pushHistory();
        this.getOpenid();
        this.stopFocus();
        this.isChecked();
        modelBox.model();//模态框
        this.submit();  //提交

        // this.byPhoneISMember(); //测试
    },
    getOpenid:function () {     //通过code获取opendid
        var url=dataModel.url.userInfo;
        $.ajax({
            url:url,
            type:"get",
            dataType:"json",
            data:{"code":code},
            timeout:10000,
            success:function (res) {
                // alert(res.data.openid)
                openid=res.data.openid;
                this.isAgainRepeat(openid);   //通过openid判断进入的页面
            }.bind(this),
            error:function(){
                window.location.href='loadSorry.html';
            }
        })
    },
    //根据openId判断是否重复入群
    isAgainRepeat:function (openid) {
        var url=dataModel.url.isRepeat;
        $.ajax({
            url:url,
            type:"post",
            dataType:"json",
            data:{
                "openid":openid,
                "type":type
            },
            timeout:10000,
            success:function (res) {
                // alert(JSON.stringify(res));
                if(res.status==200){  //status:200表示已经有该用户的信息，直接跳转到QRcode页面
                    if(res.data != null){
                        window.location.href="QRCode.html?QRcodeurl="+res.data.qr_url+"&Validate="+res.data.verify_code;
                        $(".loadingView").fadeOut();
                    }else{
                        window.location.href="sorry.html";
                    }
                }else if(res.status==2){
                    window.location.href="againRepeat.html";
                }else{
                    $(".loadingView").fadeOut();
                    this.byPhoneISMember();
                }
            }.bind(this),
            error:function(){
                window.location.href='loadSorry.html';
            }
        })
    },
    byPhoneISMember:function () {
        $("#confirmBtn").get(0).addEventListener('click',function () {
            var phoneVal = $("#phone").val();
            if(phoneVal == ""){
                alert("请填写您的手机号");
                return
            }else if(!/^1[0-9]{10}$/.test(phoneVal)){
                alert('您输入的手机号有错');
                $("#phone").val('')
                return
            }else{
                $(this).addClass('active').removeClass('activeDefault');
                $(".loadingView .loadWord").text("正在为您匹配合适的群，请稍等...")
                $(".loadingView").fadeIn();
                console.log(phoneVal)
                controller.getMemberInfo(phoneVal);
            }
        })
    },
    //根据手机号获取数据
    getMemberInfo:function (phone) {
        var url = dataModel.url. getMemberInfo;
        $.ajax({
            url:url,
            type:'post',
            data:{
                'phone':phone,
                'type': type
            },
            timeout:10000,
            success:function (res) {
                // alert(JSON.stringify(res));
                if(res.status == 200){
                    if(res.data !=null){
                        $("#province").val(res.data.province)
                        $("#city").val(res.data.city)
                        $('#city').css({'color':'#000','background':'#fff'})
                        $(".dot1").css({'borderRight': '4px solid #333', 'borderBottom': '4px solid #333'})
                        $(".predateSelect").addClass('activeColor');
                        $(".predateSelect span.edcSpan").text(res.data.edc.replace(/\-/g,'/')).css('color','#333');
                        $(".predateSelect").next('.dot').css({'borderRight': '4px solid #333', 'borderBottom': '4px solid #333'})
                        this.getProAndCity();
                        this.get_predate();
                        $(".loadingView").fadeOut();
                    }
                }else if(res.status == 1 || res.status == 2){  //1:"没有该用户信息" 2:"有多条用户信息"
                    $("input[id = " + "province" + "]").val('')
                    $("input[id = " + "city" + "]").val('')
                    $('#city').css({'color':'#000','background':'#fff'})
                    $(".dot1").css({'borderRight': '4px solid #999', 'borderBottom': '4px solid #999'})
                    $(".predateSelect").addClass('activeColor');
                    $(".predateSelect span.edcSpan").text("年／月／日").css('color','#999');
                    $(".predateSelect").next('.dot').css({'borderRight': '4px solid #999', 'borderBottom': '4px solid #999'})
                    this.getProAndCity();
                    $(".loadingView").fadeOut();

                }else{
                    window.location.href="sorry.html";
                }
            }.bind(this),
            error:function(){
                window.location.href='loadSorry.html';
            }
        })
    },
    //根据调取的接口获取城市数据，选择省市
    getProAndCity:function () {
        var url= dataModel.url.getProAndCity;
        $.ajax({
            url:url,
            type:"get",
            dataType:"json",
            data:{ 'type': type },
            success:function (data) {
                var provinceArr = new Array();
                provinceArr.splice(0, provinceArr.length);
                if (!(eval(data).length == 0)) {
                    for (var i in eval(data)) {
                        provinceArr.push({"value": eval(data)[i].value,"child":eval(data)[i].child})
                    }
                };
                // console.log(eval(cityArr))
                new MultiPicker({
                    input: 'city',//点击触发插件的input框的id
                    container: 'cityContainer',//插件插入的容器id
                    jsonData:provinceArr,
                    success: function (arr) {
                        console.log(arr)
                        var cityName=arr[1].value,province=arr[0].value;
                        controller.handleCityChange(cityName,province);
                    }//回调：确定
                });
            }
        })
    },
    //选择城市 
    handleCityChange: function (cityName, province) {
        $("input[id = " + "province" + "]").val(province)
        $("input[id = " + "city" + "]").val(cityName)
        $('#city').css({'color':'#000','background':'#fff'})
        $('#city').next('.dot1').css({'borderRight': '4px solid #333', 'borderBottom': '4px solid #333'});
        this.get_predate();
    },
    get_predate:function(){
        $("#date").attr('type','date').removeAttr('disabled')
        $("#date").get(0).addEventListener('change',function () {
            if ($(this).val() == "" && $(".predateSelect span.edcSpan").text() == "") {
                $(".predateSelect span.edcSpan").text("年／月／日");
            }else{
                $(".predateSelect span.edcSpan").text($(this).val().replace(/\-/g,"/")).css('color','#333');
                $(".predateSelect").addClass('activeColor')
                $(".predateSelect").next('.dot').css({'borderRight': '4px solid #333', 'borderBottom': '4px solid #333'})
            }
        })
    },
    submit:function () {
        $("#submitBtn").get(0).addEventListener("click",function () {
            cookieFunction.setCookie();
        })
    },
    isChecked:function () {
        $(".Checked").click(function () {
            if($(this).attr("state")=="false"){
                $(this).attr("state","true").addClass("privacyCheckedActive").removeClass("privacyChecked");
                $("#submitBtn").removeAttr("disabled").addClass("bgColor_blue").removeClass("bgColor_grey");
            }else{
                $(this).attr("state","false").addClass("privacyChecked").removeClass("privacyCheckedActive");
                $("#submitBtn").attr("disabled",'disabled').addClass("bgColor_grey").removeClass("bgColor_blue");
            }
        })
    },

    pushHistory:function(){
        var bool = false;
        setTimeout(function () {
            bool = true;
        }, 1500);
        window.addEventListener("popstate", function () {
            if (bool) {
                window.location.reload();//根据自己的需求实现自己的功能
            }
            this.pushHistory();
        }, false);
        var state = {
            title: "title",
            url: "#"
        }
        window.history.replaceState(state, "title", "#");
    },
    stopFocus:function () {
        $("input[disabled]").focus(function () {
            document.documentElement.blur();
        });
    }
}
var cookieFunction = {
    setCookie: function () {
        var judgeFlag = true;
        var cookieData = new Object();        // 类似于var cookieDate={}创建对象
        var allInput = document.querySelectorAll("input[class='content_info']");
        if($("#city").val()=='' && $("#province").val() ==''){
            alert('请选择城市');
            return false;
        }
        var edcText = $(".predateSelect span.edcSpan").text();
        if(edcText != "" && edcText != '年／月／日'){
            cookieData.edc=edcText;
        }else{
            alert("请选择预产期");
            return false;
        }
        $.each(allInput, function (k, v) {
            if (v.value == '') {
                judgeFlag = false;
                return false;
            } else {
                cookieData[v.name] = v.value;
            }
        });
        cookieData.openid=openid;
        cookieData.type=type;
        try {
            // 判断省、市是否有值（非空判断）
            if (judgeFlag != false) {    //判断成功之后点击按钮进入到页面
                window.location.href= 'QRCode.html';
            } else {
                throw "请完善信息"
            }
        } catch (err) {
            alert(err);
            return false;
        }
        // 存入cookie
        $.cookie("data",JSON.stringify(cookieData),{"expires":1});
    }
}
var modelBox={
    model:function () {
        $("#privacyBtn").get(0).addEventListener("click",function(){
            $(".model").removeClass("fade");
        });
        $("#closeIcon").get(0).addEventListener("click",function(){
            $(".model").addClass("fade");
        });
        $("#modelBtn").get(0).addEventListener("click",function(){
            $("#privacyChecked").prop("checked", false);
            if($(".Checked").attr("state")=="false")
                $(".Checked").trigger("click");
            $(".model").addClass("fade");
        });
    }
}

function get_predate(obj){
    if (obj.value == "" && $(".predateSelect span.edcSpan").text() == "") {
        $(".predateSelect span.edcSpan").text("年／月／日");
    }else{
        $(".predateSelect span.edcSpan").text(obj.value.replace(/\-/g,"/")).css('color','#333');
        $(".predateSelect").addClass('activeColor')
        $(".predateSelect").next('.dot').css({'borderRight': '4px solid #333', 'borderBottom': '4px solid #333'})

    }
}

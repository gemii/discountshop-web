/**
 * Created by mavis on 2017/5/26.
 */
var getQRcodeFunction={
    init:function () {    //页面在初始化时获取数据，获取img图片的地址
        document.getElementById("QR-view").style.height=document.documentElement.clientHeight+"px";
        this.ISrobotid(); //二维码生产
    },
    ISrobotid:function(){
        var QRcodeurl=getQueryString("QRcodeurl");//二维码链接
        var Validate=getQueryString("Validate"); //验证码
        if(QRcodeurl!=undefined && QRcodeurl!="" && QRcodeurl!=null && Validate!=undefined && Validate!="" && Validate!=null){
            $(".QR-codeIMG").attr("src",QRcodeurl);
            $("#validateCode").get(0).innerHTML=Validate;
            $(".loadingView").fadeOut();
            $(".main").css('display','block');
        }else {
            this.getCookie(); //postRobotId
        }
    },
    getCookie:function () {
        var current;
        if(($.cookie("data"))!=undefined){
            current=JSON.parse($.cookie("data"));
        }
        console.log(current)
        this.getQRUrlAndCode(current); //获取robotid值
    },
    getQRUrlAndCode:function (current) {
        console.log(JSON.stringify(current));
        var url=dataModel.url.getCodeUrl;
        $.ajax({
            url:url,
            type:"post",
            dataType: 'json',
            async:true,      // 异步请求
            timeout:10000,
            data:{
                "openid":current.openid,
                "edc":current.edc.replace(/\//g,"-"),
                "phone":current.phone,
                "province":current.province,
                "city":current.city,
                "type":current.type,
            },
            success:function (res) {
                console.log(res.data)
                if(res.status==200){
                    //成功之后获取robotid,拼接图片地址，赋值
                    $(".QR-codeIMG").attr("src",res.data.robotQr);
                    //验证码
                    $("#validateCode").get(0).innerHTML=res.data.code;
                    $(".loadingView").fadeOut();
                    $(".main").css('display',' block');
                }else{
                    window.location.href="sorry.html";
                }
            },
            error:function(){
                window.location.href='loadSorry.html';
            }
        })
    },

}
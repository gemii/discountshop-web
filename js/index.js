/**
 * Created by mavis on 2017/6/9.
 */
//mt测试环境
var wxIp = "http://wx.gemii.cc/";
var groupIp = "http://mt.gemii.cc/";
var dataModel= {
    url: {
        userInfo: wxIp + "wx/base/getUserInfo",                          //通过code值获取openid
        isRepeat: groupIp + "HelperManage/enter/select/robotUrl",       //判断是否重复入群
        getMemberInfo:groupIp +"HelperManage/gemii/selectUser",         //根据手机号获取会员信息
        getProAndCity:groupIp + "HelperManage/gemii/provinces/citys",   //根据调取的接口获取城市数据，选择省
        getCodeUrl: groupIp + "HelperManage/gemii/selectGroup",         //获取二维码地址和验证码的接口
    },
}
